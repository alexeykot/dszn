import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import LoginScreen from './src/screens/LoginScreen';
import IntroScreen from './src/screens/IntroScreen';
import WidgetScreen from './src/screens/WidgetScreen';

const Stack = createStackNavigator();

export default function MainApp() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{ gestureEnabled: false }}
        headerMode="none">
        {/* <Stack.Screen name="WidgetScreen" component={WidgetScreen} /> */}
        <Stack.Screen name="IntroScreen" component={IntroScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="WidgetScreen" component={WidgetScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
