export default {
  primary: '#F4F4F6',
  white: '#FFF',
  lighter: '#F3F3F3',
  light: '#DAE1E7',
  dark: '#444',
  black: '#000',
  lightGray: '#D5D6D8',
  red: 'red',
};
