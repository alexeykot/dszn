/**
 * @format
 */
import { AppRegistry } from 'react-native';
import Screens from './Screens';
import { name as appName } from './app.json';
import 'react-native-gesture-handler';

AppRegistry.registerComponent(appName, () => Screens);
