import React from 'react';

// import Header from './components/Header';

import styles from './styled';
import { WebView } from 'react-native-webview';
import { View } from 'react-native';

const url = 'http://heirarchy.dtszn.dev-vps.ru/mobile/default/index';
const IntroScreen = (props) => {
  React.useEffect(() => {
    setTimeout(() => props.navigation.navigate('LoginScreen'), 2000);
  });
  return (
    <View style={styles.main}>
      <WebView
        source={{
          uri: url,
        }}
        cacheEnabled={false}
      />
    </View>
  );
};

export default IntroScreen;
