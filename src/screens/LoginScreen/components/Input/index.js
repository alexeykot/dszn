import React from 'react';
import { View, TextInput, Text } from 'react-native';

import styles from './styled';

const config = {
  email: 'emailAddress',
  password: 'password',
};
const Input = ({ placeholder, type, onChangeText, value, error }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.placeholder}>{placeholder}</Text>
      <TextInput
        textContentType={config[type]}
        secureTextEntry={type === 'password'}
        onChangeText={(text) => onChangeText(text, type)}
        value={value}
        style={error && error.length ? styles.inputError : styles.input}
      />
      {error && error.length ? (
        <Text style={styles.error}>{error[0]}</Text>
      ) : null}
    </View>
  );
};

export default Input;
