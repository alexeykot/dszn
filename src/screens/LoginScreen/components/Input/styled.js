import { StyleSheet } from 'react-native';
import Colors from '../../../../../colors';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 20,
  },
  input: {
    width: '100%',
    height: 54,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#DDDDDF',
    backgroundColor: Colors.white,
  },
  inputError: {
    width: '100%',
    height: 54,
    borderRadius: 4,
    borderWidth: 1,
    backgroundColor: Colors.white,
    borderColor: Colors.red,
  },
  placeholder: {
    fontSize: 14,
    color: Colors.black,
    marginBottom: 7,
  },
  error: {
    fontSize: 14,
    color: Colors.red,
  },
});

export default styles;
