import React from 'react';
import http from '../../services/http/http';
import {
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';

import logo from '../../assets/images/logo.png';

import styles from './styled';
import Input from './components/Input';

const config = [
  {
    type: 'login',
    placeholder: 'Электронная почта',
  },
  {
    type: 'password',
    placeholder: 'Пароль',
  },
];

const LoginScreen = (props) => {
  const [loading, setLoading] = React.useState(false);
  const [credentials, setCredentials] = React.useState({
    login: 'awesome.fortesting@yandex.ru',
    password: '11111111',
    // login: '',
    // password: '',
  });
  const [errors, setErrors] = React.useState({
    login: [],
    password: [],
  });
  const onChangeText = (text, type) =>
    setCredentials({
      ...credentials,
      [type]: text,
    });
  const onButtonPress = async () => {
    setLoading(true);
    try {
      const { data } = await http.post(
        '/cabinet/authorization/index',
        credentials,
      );
      console.log('response', data);
      props.navigation.navigate('WidgetScreen', {
        token: data.data.entity['access-token'],
        url: `https://dtszn.dev-vps.ru/mobile/cabinet/widget/category?access-token=${data.data.entity['access-token']}`,
      });
      // alert('Залогинен!');
    } catch (err) {
      const error = JSON.parse(err.response.data.data.message);
      console.log('error', error, credentials);
      setErrors(error);
    }
    setLoading(false);
  };
  const isEnabled = Object.values(credentials).every((value) => value !== '');
  return (
    <ScrollView s contentContainerStyle={styles.main}>
      <Image source={logo} />
      <Text style={styles.subtitle}>
        Департамент труда и социальной защиты населения города Москвы
      </Text>
      <Text style={styles.title}>Авторизация</Text>
      {config.map((input) => (
        <Input
          key={input.type}
          onChangeText={onChangeText}
          error={errors[input.type]}
          value={credentials[input.type]}
          placeholder={input.placeholder}
          type={input.type}
        />
      ))}
      <TouchableOpacity
        disabled={!isEnabled}
        onPress={onButtonPress}
        style={[styles.button, isEnabled && styles.buttonFilled]}>
        {!loading ? (
          <Text
            style={[styles.buttonText, isEnabled && styles.buttonTextFilled]}>
            Войти
          </Text>
        ) : (
          <ActivityIndicator size="large" color="white" />
        )}
      </TouchableOpacity>
    </ScrollView>
  );
};

export default LoginScreen;
