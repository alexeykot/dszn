import { StyleSheet } from 'react-native';
import Colors from '../../../colors';

const styles = StyleSheet.create({
  main: {
    backgroundColor: Colors.primary,
    paddingHorizontal: 21,
    paddingTop: 40,
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 14,
    textAlign: 'center',
    color: Colors.black,
    marginTop: 12,
  },
  title: {
    fontSize: 20,
    color: Colors.black,
    marginTop: 41,
  },
  button: {
    width: '100%',
    height: 54,
    backgroundColor: Colors.lightGray,
    marginTop: 35,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonFilled: {
    backgroundColor: Colors.red,
  },
  buttonText: {
    color: Colors.black,
    fontSize: 16,
  },
  buttonTextFilled: {
    color: Colors.white,
  },
});

export default styles;
