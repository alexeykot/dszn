import React from 'react';

// import Header from './components/Header';

import styles from './styled';
import { WebView } from 'react-native-webview';
import { View } from 'react-native';

const url = 'http://heirarchy.dtszn.dev-vps.ru/mobile/default/index';
const WebViewScreen = (props) => {
  // const { title, url } = props;
  const webviewRef = React.useRef(null);
  const [key, setKey] = React.useState(1);
  React.useEffect(() => {
    return function cleanup() {
      setKey(key + 1);
    };
  });
  // const haveSymbol = url.includes('?');
  return (
    <View style={styles.main}>
      <WebView
        // key={key}
        source={{
          // uri: `${url}${haveSymbol ? '&' : '?'}access-token=${props.token}`,
          uri: url,
          // headers: {
          //   'X-Auth-Token': props.token,
          // },
        }}
        cacheEnabled={false}
        ref={webviewRef}
      />
    </View>
  );
};

export default WebViewScreen;
