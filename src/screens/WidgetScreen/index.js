import React from 'react';

import styles from '../styled';
import { WebView } from 'react-native-webview';
import { View } from 'react-native';
import {
  handleAndroidBackButton,
  exitAlert,
  removeAndroidBackButtonHandler,
} from '../../helpers';

const WidgetScreen = (props) => {
  React.useEffect(() => {
    handleAndroidBackButton(exitAlert);
    return removeAndroidBackButtonHandler;
  });
  return (
    <View style={styles.main}>
      <WebView
        source={{
          uri: props.route.params.url,
          // headers: {
          //   'X-Auth-Token': props.route.params.token,
          // },
        }}
        // cacheEnabled={false}
      />
    </View>
  );
};

export default WidgetScreen;
