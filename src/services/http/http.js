import axios from 'axios';

const baseUrl = 'https://dtszn.dev-vps.ru/mobile/api/v1';

const http = axios.create({
  withCredentials: false,
  baseURL: baseUrl,
  headers: { 'Content-Type': 'application/json' },
});

export default http;
